package cz.cvut.fel.ts1;


import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FactorialTest {


    static Factorial f;

    @BeforeAll
    public static void initVariable() {
        f = new Factorial();
    }

    @Test
    public void getFactorial_1() {
        //ARRANGE
        Factorial foo = new Factorial();

        //ACT
        int num = 1;

        //ASSERT
        Assertions.assertEquals(1, num);
    }


    @Test
    @Order(1)
    public void get_factorial_2() {
        //ACT


        //ASSERT
        Assertions.assertEquals(2,  f.getFactorial(2));
    }

    @Test
    @Order(2)
    public void get_factorial_3() {
        //ACT


        //ASSERT
        Assertions.assertEquals(6,  f.getFactorial(3));
    }
    @Test
    @Order(3)
    public void get_factorial_4() {
        //ACT


        //ASSERT
        Assertions.assertEquals(24,  f.getFactorial(4));
    }
    @Test
    @Order(4)
    public void get_factorial_0() {
        //ACT


        //ASSERT
        Assertions.assertEquals(1,  f.getFactorial(0));
    }
    @Test
    @Order(5)
    public void get_factorial_10() {
        //ACT


        //ASSERT
        Assertions.assertEquals(3628800,  f.getFactorial(10));
    }

}
